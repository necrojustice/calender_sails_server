/**
 * Sails Seed Settings
 * (sails.config.seeds)
 *
 * Configuration for the data seeding in Sails.
 *
 * For more information on configuration, check out:
 * http://github.com/frostme/sails-seed
 */
module.exports.seeds = {
	project: [
		{
        "name": "Game Gamer Website"
      },
      {
        "name": "Buildings R Us Redesign"
      }
	],
	user: [
	    {
	    "email": "delacruzcampos@zaphire.com",
	    "password": "password"
	  },
	  {
	    "email": "mariettacampos@zaphire.com",
	    "password": "password"
	  }
  ]
}
